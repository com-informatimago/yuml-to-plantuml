;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               plantuml-generator.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;    
;;;;    Generates PlantUML diagrams.
;;;;    
;;;;AUTHORS
;;;;    <PJB> Pascal J. Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2017-01-05 <PJB> Created.
;;;;BUGS
;;;;LEGAL
;;;;    AGPL3
;;;;    
;;;;    Copyright Pascal J. Bourguignon 2017 - 2017
;;;;    
;;;;    This program is free software: you can redistribute it and/or modify
;;;;    it under the terms of the GNU Affero General Public License as published by
;;;;    the Free Software Foundation, either version 3 of the License, or
;;;;    (at your option) any later version.
;;;;    
;;;;    This program is distributed in the hope that it will be useful,
;;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;    GNU Affero General Public License for more details.
;;;;    
;;;;    You should have received a copy of the GNU Affero General Public License
;;;;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;**************************************************************************
(eval-when (:compile-toplevel :load-toplevel :execute)
  (setf *readtable* (copy-readtable nil)))
(defpackage "COM.INFORMATIMAGO.YUML-TO-PLANTUML.PLANTUML-GENERATOR"
  (:nicknames "PG")
  (:use "COMMON-LISP"
        "COM.INFORMATIMAGO.COMMON-LISP.CESARUM.UTILITY"
        "COM.INFORMATIMAGO.COMMON-LISP.CESARUM.LIST"
        "COM.INFORMATIMAGO.YUML-TO-PLANTUML.UML")
  (:export "GENERATE-CLASS-DIAGRAM"
           "GENERATE-ACTIVITY-DIAGRAM"
           "GENERATE-USECASE-DIAGRAM"
           "TRANSFORM-NON-BINARY-DECISIONS-AS-TRANSITIONS")
  (:documentation "Generates PlantUML diagframs."))
(in-package "COM.INFORMATIMAGO.YUML-TO-PLANTUML.PLANTUML-GENERATOR")

(defvar *generated*)
(defmacro with-generation (&body body)
  `(with-standard-io-syntax
     (let ((*generated*      (make-hash-table))
           (*print-circle*   nil)
           (*print-readably* nil))
       ,@body
       (values))))
(defun already-generated-p (object)
  (nth-value 1 (gethash object *generated*)))
(defun generated (object)
  (setf (gethash object *generated*) t))
(defmacro generating (object &body body)
  (let ((vobject (gensym)))
    `(let ((,vobject ,object))
       (unless (already-generated-p ,vobject)
         (generated ,vobject)
         ,@body))))



(defgeneric generate (entity stream))

(defmethod generate (entity stream)
  (declare (ignore stream))
  (break)
  (format *error-output* "Generating ~S is not implemented yet.~%" entity))

(defun escape (string)
  "
~% -> \n
:  -> \:
)  -> \)
"
  string)


#-(and) (
         (defclass uml-named-entity ()
           ((name       :initarg :name       :accessor uml-name)))
         (defclass uml-label ()
           ((label      :initarg :label      :accessor uml-label)))
         (defclass uml-bracketed-label (uml-label)
           ())

         (defclass uml-note ()
           ((text :initarg :text :accessor uml-note-text)))

         )

;;; Class diagrams

(defmethod generate ((class uml-class) stream)
  (generating class
    (format stream "~@[abstract ~]class ~A {~%~
                    ~:{    ~A ~A~%~}~
                    ~:{    ~A ~A~%~}~
                    }~%"
            (search "<<abstract>>" (uml-stereotype class))
            (escape (remove-if-not (function alphanumericp) (uml-name class)))
            (mapcar (lambda (attribute)
                      (list (getf (rest attribute) :type t)
                            (first attribute)))
                    (uml-class-attributes class))
            (uml-class-operations class))))

(defmethod uml-id ((class uml-class))
  (remove-if-not (function alphanumericp) (uml-name class)))

(defmethod generate ((generalization uml-generalization) stream)
  (generating generalization
    (generate (uml-generalization-superclass generalization) stream)
    (generate (uml-generalization-subclass   generalization) stream)
    (format stream "~A <|-- ~A~%"
            (uml-id (uml-generalization-superclass generalization))
            (uml-id (uml-generalization-subclass   generalization)))))

(defun line (line)
  (ecase line
    ((:solid)  "---")
    ((:dashed) "...")))

(defun multiplicity (multiplicity)
  (flet ((range (mult)
           (if (consp mult)
               (format nil "~A..~A" (car mult) (cdr mult))
               mult)))
    (cond
      ((atom multiplicity)
       (format nil "\"~A\"" multiplicity))
      ((atom (cdr multiplicity))
       (format nil "\"~A\"" (range multiplicity)))
      (t
       (case (length multiplicity)
         (0         nil)
         (1         (format nil "\"~A\""      (mapcar (function range) multiplicity)))
         (otherwise (format nil "\"~:{~A~}\"" (mapcar (function range) multiplicity))))))))

(defun arrow (arrow)
  (if arrow
      (format nil "~A" arrow)
      ""))

(defun diamond (diamond)
  (case diamond
    (:odiamond "o")
    (:diamond  "++")
    (otherwise "")))

(defmethod generate ((association uml-association) stream)
  (generating association
    (generate (uml-association-left-class  association) stream)
    (generate (uml-association-right-class association) stream)
    (format stream "~A ~@[~A ~]~A~A~A~A~A~@[ ~A~] ~A~%"
            (uml-id       (uml-association-left-class         association))
            (multiplicity (uml-association-left-multiplicity  association))
            (arrow        (uml-association-left-arrow         association))
            (diamond      (uml-association-left-diamond       association))
            (line         (uml-association-line               association))
            (diamond      (uml-association-right-diamond      association))
            (arrow        (uml-association-right-arrow        association))
            (multiplicity (uml-association-right-multiplicity association))
            (uml-id       (uml-association-right-class        association)))))

;;; Activity Diagrams



(defmethod uml-name ((activity uml-activity))
  (let ((name (call-next-method)))
    (if (or (string= name "start")
            (string= name "end"))
        "*"
        name)))

(defmethod uml-id ((activity uml-activity))
  (let ((name (uml-name activity)))
    (if (string= "*" name)
        "()"
        (call-next-method))))

(defmethod generate ((activity uml-activity) stream)
  (let ((name (uml-name activity)))
    (if (already-generated-p activity)
        (format stream "~A"  (uml-id activity))
        (generating activity
          (if (string= "*" name)
              (format stream "() ")
              (format stream "~S as ~A~%" (escape name) (uml-id activity)))))))

(defmethod generate ((parallel uml-parallel) stream)
  (format stream "===~A=== " (uml-id parallel)))

(defmethod generate ((decision  uml-decision) stream)
  (flet ((generate-decision ()
           (generating decision
             (format stream "if ~S then~%" (escape (uml-name decision)))
             (let ((transitions-from (uml-transitions-from decision)))
               (unless (= 2 (length transitions-from))
                 (error "Expected 2 transtions from the decision ~A, found ~D."
                        (uml-name decision)  (length transitions-from)))
               (generate (first transitions-from) stream)
               (format stream "else~%")
               (generate (second transitions-from) stream))
             (format stream "endif~%")))
         (generate-from-from ()
           (generate (first (uml-transitions-to decision)) stream)))
    (case (length (uml-transitions-to decision))
      ((0)
       (generate-decision))
      ((1)
       (generate-decision)
       ;; (if (typep (first (uml-transitions-to decision)) 'uml-decision)
       ;;     (generate-decision)
       ;;     ;; A little hack. It seems the diagrams we're converting are using
       ;;     ;; "decisions" instead of mere transitions from the previous node.
       ;;     ;; Therefore we correct it by generating the previous node instead.
       ;;     (generate-from-from))
       )
      (otherwise
       (error "Too many transitions to the decision ~A, found ~D."
              (uml-name decision) (length (uml-transitions-to decision)))))))

(defmethod generate ((label uml-label) stream)
  (format stream "[~A]" (escape (uml-label label))))

(defmethod generate ((label uml-bracketed-label) stream)
  (format stream "~A" (escape (uml-label label))))


(defmethod generate ((transition uml-transition) stream)
  (generating transition
    (generate (uml-transition-from transition) stream)
    (format stream " --> ")
    (when (uml-transition-from-label transition)
      (generate (uml-transition-from-label transition) stream)
      (format stream " "))
    (when (uml-transition-to-label transition)
      (generate (uml-transition-to-label transition) stream)
      (format stream " "))
    (generate (uml-transition-to   transition) stream)
    (format stream "~%")))


;;; Usecase Diagrams

(defmethod generate ((actor uml-actor) stream)
  (generating actor
    (format stream ":~A: as ~A~@[ ~A~]~%"
            (escape (uml-name actor))
            (uml-id actor)
            (uml-stereotype actor))))

(defmethod uml-id ((usecase uml-usecase))
  (format nil "(~A)" (call-next-method)))

(defmethod generate ((usecase uml-usecase) stream)
  (generating usecase
    (format stream "(~A) as ~A~@[ ~A~]~%"
            (escape (uml-name usecase))
            (uml-id usecase)
            (uml-stereotype usecase))))

(defmethod generate ((uses uml-usecase-uses) stream)
  (generating uses
    (generate (uml-usecase-user uses) stream)
    (generate (uml-usecase-used uses) stream)
    (format stream "~A -- ~A~%"
            (uml-id (uml-usecase-user uses))
            (uml-id (uml-usecase-used uses)))))
(defmethod generate ((extend uml-usecase-extends) stream)
  (generating extend
    (generate (uml-usecase-extended extend) stream)
    (generate (uml-usecase-extending extend) stream)
    (format stream "~A .> ~A : extends~%"
            (uml-id (uml-usecase-extending extend))
            (uml-id (uml-usecase-extended extend)))))
(defmethod generate ((include uml-usecase-includes) stream)
  (generating include
    (generate (uml-usecase-included include) stream)
    (generate (uml-usecase-including include) stream)
    (format stream "~A .> ~A : includes~%"
            (uml-id (uml-usecase-including include))
            (uml-id (uml-usecase-included include)))))
(defmethod generate ((generalization uml-usecase-generalization) stream)
  (generating generalization
    (generate (uml-usecase-subusecase generalization) stream)
    (generate (uml-usecase-superusecase generalization) stream)
    (format stream "~A <|-- ~A~%"
            (uml-id (uml-usecase-superusecase generalization))
            (uml-id (uml-usecase-subusecase generalization)))))
(defmethod generate ((generalization uml-actor-generalization) stream)
  (generating generalization
    (generate (uml-usecase-subactor generalization) stream)
    (generate (uml-usecase-superactor generalization) stream)
    (format stream "~A <|-- ~A~%"
            (uml-id (uml-usecase-superactor generalization))
            (uml-id (uml-usecase-subactor generalization)))))


(defun generate-header (stream)
  (format stream "~%@startuml~2%"))

(defun generate-footer (stream)
  (format stream "~%@enduml~%"))

(defun generate-usecase-diagram (diagram &optional (stream *standard-output*))
  (with-generation
    (generate-header stream)
    (dolist (phrase diagram)
      (dolist (object phrase)
        (generate object stream)))
    (generate-footer stream)))

(defun generate-class-diagram (diagram &optional (stream *standard-output*))
  (with-generation
    (generate-header stream)
    (dolist (object (flatten diagram))
      (generate object stream))
      (generate-footer stream)))


(defgeneric guard-and (a b)
  (:method ((a uml-label) (b uml-label))
    (make-instance 'uml-bracketed-label
                   :label (format nil "[~A AND ~A]" (uml-label a) (uml-label b))))
  (:method ((a null) (b uml-label))
    b)
  (:method ((a uml-label) (b null))
    a)
  (:method ((a null) (b null))
    nil))

(defmethod ellide-decision ((decision uml-decision))
  (assert (= 1 (length (uml-transitions-to   decision))))
  (assert (< 2 (length (uml-transitions-from decision))))
  (let* ((transitions      (uml-transitions-from decision))
         (to-transition    (first (uml-transitions-to decision)))
         (from-node        (uml-transition-from       to-transition))
         (from-guard       (uml-transition-from-label to-transition))
         (to-guard         (uml-transition-to-label   to-transition)))
    (remove-transition-to from-node to-transition)
    (dolist (transition transitions)
      (setf (uml-transition-from       transition) from-node
            (uml-transition-from-label transition) (guard-and from-guard (uml-transition-from-label transition))
            (uml-transition-to-label   transition) (guard-and to-guard   (uml-transition-to-label   transition)))
      (add-transition-to from-node transition))))

(defun flatten-diagram (diagram)
  (mapcan (lambda (object)
            (if (typep object 'uml-transition)
                (list (uml-transition-from object)
                      (uml-transition-to object)
                      object)
                (list object)))
          (flatten diagram)))

(defun transform-non-binary-decisions-as-transitions (objects)
  (loop :for decision :in objects
        :when (and (typep decision 'uml-decision)
                   (= 1 (print (length (uml-transitions-to   decision))))
                   (< 2 (print (length (uml-transitions-from decision)))))
          :do (ellide-decision decision)
              (setf objects (delete decision objects))
        :finally (return objects)))

(defun generate-activity-diagram (diagram &optional (stream *standard-output*))
  (let* ((flat        (flatten-diagram diagram))
         (transformed (transform-non-binary-decisions-as-transitions flat)))
    (pprint diagram)
    (pprint flat)
    (pprint transformed)
    (pprint (set-difference flat transformed))
    (with-generation
      (generate-header stream)
      (dolist (object transformed)
        (generate object stream))
      (generate-footer stream))))


;;;; THE END ;;;;
