;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               yuml-to-plantuml.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;    
;;;;    Converts YUML diagrams to PlantUML.
;;;;    
;;;;AUTHORS
;;;;    <PJB> Pascal J. Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2017-01-05 <PJB> Created.
;;;;BUGS
;;;;LEGAL
;;;;    AGPL3
;;;;    
;;;;    Copyright Pascal J. Bourguignon 2017 - 2017
;;;;    
;;;;    This program is free software: you can redistribute it and/or modify
;;;;    it under the terms of the GNU Affero General Public License as published by
;;;;    the Free Software Foundation, either version 3 of the License, or
;;;;    (at your option) any later version.
;;;;    
;;;;    This program is distributed in the hope that it will be useful,
;;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;    GNU Affero General Public License for more details.
;;;;    
;;;;    You should have received a copy of the GNU Affero General Public License
;;;;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;**************************************************************************
(eval-when (:compile-toplevel :load-toplevel :execute)
  (setf *readtable* (copy-readtable nil)))
(defpackage "COM.INFORMATIMAGO.YUML-TO-PLANTUML"
  (:use "COMMON-LISP"
        "COM.INFORMATIMAGO.COMMON-LISP.CESARUM.UTILITY"
        "COM.INFORMATIMAGO.COMMON-LISP.CESARUM.FILE"
        "COM.INFORMATIMAGO.YUML-TO-PLANTUML.UML"
        "COM.INFORMATIMAGO.YUML-TO-PLANTUML.YUML-PARSER"
        "COM.INFORMATIMAGO.YUML-TO-PLANTUML.PLANTUML-GENERATOR")
  (:export "CONVERT")
  (:documentation "Converts YUML diagrams to PlantUML."))
(in-package "COM.INFORMATIMAGO.YUML-TO-PLANTUML")

(defparameter *plantuml* "/Users/pjb/src/java/plantuml-8053/plantuml")

(defun plantuml (puml-file)
  (ensure-directories-exist "puml")
  (asdf:run-shell-command
   (format nil "~A ~A -output puml" *plantuml* puml-file)))

(defun convert (yuml-file &key (show-result nil))
  (reset-interned-objects)
  (let* ((name      (pathname-name yuml-file))
         (dispatch  '(("s-" generate-activity-diagram parse-activity-diagram)
                      ("o-" generate-class-diagram    parse-class-diagram)
                      ("u-" generate-usecase-diagram  parse-usecase-diagram)))
         (entry     (assoc (subseq name 0 (min (length name) 2)) dispatch :test (function string=)))
         (puml-file (make-pathname :type "puml" :defaults yuml-file)))
    (with-open-file (output puml-file
                            :direction :output
                            :if-does-not-exist :create
                            :if-exists :supersede)
      (funcall (second entry)
               (funcall (third entry) (text-file-contents yuml-file))
               output))
    (plantuml puml-file)
    (when show-result
      (asdf:run-shell-command "open ~A ~A"
                              (make-pathname :type "png"
                                             :defaults yuml-file)
                              (make-pathname :directory '(:relative "puml")
                                             :type "png"
                                             :defaults puml-file)))))


;; (let* ((yuml-file (first  (directory "u-*.yuml")))
;;        (puml-file (make-pathname :type "puml" :defaults yuml-file)))
;;   (values yuml-file puml-file
;;           (make-pathname :type "puml" :defaults yuml-file)
;;           (make-pathname :directory '(:relative "puml")
;;                          :type "png"
;;                          :defaults puml-file)))


;;;; THE END ;;;;
