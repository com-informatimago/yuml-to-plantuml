;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               yuml-parser.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;    
;;;;    Parser for yuml.me files.
;;;;    
;;;;AUTHORS
;;;;    <PJB> Pascal J. Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2016-12-30 <PJB> Created.
;;;;BUGS
;;;;LEGAL
;;;;    AGPL3
;;;;    
;;;;    Copyright Pascal J. Bourguignon 2016 - 2016
;;;;    
;;;;    This program is free software: you can redistribute it and/or modify
;;;;    it under the terms of the GNU Affero General Public License as published by
;;;;    the Free Software Foundation, either version 3 of the License, or
;;;;    (at your option) any later version.
;;;;    
;;;;    This program is distributed in the hope that it will be useful,
;;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;    GNU Affero General Public License for more details.
;;;;    
;;;;    You should have received a copy of the GNU Affero General Public License
;;;;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;**************************************************************************
(eval-when (:compile-toplevel :load-toplevel :execute)
  (setf *readtable* (copy-readtable nil)))
(defpackage "COM.INFORMATIMAGO.YUML-TO-PLANTUML.UML"
  (:use "COMMON-LISP")
  (:export
   "ADD-TRANSITION-TO"
   "ADD-TRANSITION-FROM"
   "REMOVE-TRANSITION-TO"
   "REMOVE-TRANSITION-FROM"
   "UML-ACTIVITY"
   "UML-ACTIVITY-NODE"
   "UML-TRANSITIONS-TO"
   "UML-TRANSITIONS-FROM" 
   "UML-ACTOR"
   "UML-ACTOR-GENERALIZATION"
   "UML-ASSOCIATION"
   "UML-ASSOCIATION-LEFT-ARROW"
   "UML-ASSOCIATION-LEFT-CLASS"
   "UML-ASSOCIATION-LEFT-DIAMOND"
   "UML-ASSOCIATION-LEFT-MULTIPLICITY"
   "UML-ASSOCIATION-LINE"
   "UML-ASSOCIATION-RIGHT-ARROW"
   "UML-ASSOCIATION-RIGHT-CLASS"
   "UML-ASSOCIATION-RIGHT-DIAMOND"
   "UML-ASSOCIATION-RIGHT-MULTIPLICITY"
   "UML-BRACKETED-LABEL"
   "UML-CLASS"
   "UML-CLASS-ATTRIBUTES"
   "UML-CLASS-OPERATIONS"
   "UML-DECISION"
   "UML-DECISION-TRANSITIONS-FROM"
   "UML-GENERALIZATION"
   "UML-GENERALIZATION-SUBCLASS"
   "UML-GENERALIZATION-SUPERCLASS"
   "UML-ID"
   "UML-LABEL"
   "UML-NAME"
   "UML-NAMED-ENTITY"
   "UML-NOTE"
   "UML-NOTE-TEXT"
   "UML-PARALLEL"
   "UML-STEREOTYPE"
   "UML-TRANSITION"
   "UML-TRANSITION-FROM"
   "UML-TRANSITION-FROM-LABEL"
   "UML-TRANSITION-TO"
   "UML-TRANSITION-TO-LABEL"
   "UML-USECASE"
   "UML-USECASE-EXTENDED"
   "UML-USECASE-EXTENDING"
   "UML-USECASE-EXTENDS"
   "UML-USECASE-GENERALIZATION"
   "UML-USECASE-INCLUDED"
   "UML-USECASE-INCLUDES"
   "UML-USECASE-INCLUDING"
   "UML-USECASE-SUBACTOR"
   "UML-USECASE-SUBUSECASE"
   "UML-USECASE-SUPERACTOR"
   "UML-USECASE-SUPERUSECASE"
   "UML-USECASE-USED"
   "UML-USECASE-USER"
   "UML-USECASE-USES"
   )
  (:export
   "RESET-INTERNED-OBJECTS" "INTERN-OBJECT")
  (:documentation "A quick & dirty UML metamodel."))
(in-package "COM.INFORMATIMAGO.YUML-TO-PLANTUML.UML")

(defvar *interned-objects* (make-hash-table))
(defun reset-interned-objects ()
  (setf *interned-objects* (make-hash-table)))
(defun intern-object (class &rest arguments &key name &allow-other-keys)
  (let ((table (or (gethash class *interned-objects*)
                   (setf (gethash class *interned-objects*)
                         (make-hash-table :test (function equal))))))
    (or (gethash name table)
        (setf (gethash name table)
              (apply (function make-instance) class arguments)))))


(defclass uml-named-entity ()
  ((name       :initarg :name       :accessor uml-name)
   (stereotype :initarg :stereotype :accessor uml-stereotype
               :initform nil)
   (id         :initarg :id         :reader   uml-id
               :initform (string (gensym)))))

(defmethod initialize-instance :after ((self uml-named-entity) &key name (stereotype nil stereotypep) &allow-other-keys)
  (declare (ignorable stereotype))
  (unless stereotypep
    (let ((send-2 (search ">>" name)))
      (when (and (string= "<<" name :end2 (min 2 (length name)))
                 send-2)
        (setf (uml-stereotype self) (subseq name 0 (+ 2 send-2))
              (uml-name self)       (string-trim "; " (subseq name (+ 2 send-2))))))))

(defclass uml-label ()
  ((label      :initarg :label      :accessor uml-label)))
(defclass uml-bracketed-label (uml-label)
  ())

(defmethod print-object ((entity uml-named-entity) stream)
  (print-unreadable-object (entity stream :identity t :type t)
    (format stream "~A" (uml-name entity)))
  entity)

(defmethod print-object ((label uml-label) stream)
  (print-unreadable-object (label stream :identity t :type t)
    (format stream "~A" (uml-label label)))
  label)

;;; Class Diagrams

(defclass uml-class (uml-named-entity)
  ((attributes :initarg :attributes :accessor uml-class-attributes :initform '())
   (operations :initarg :operations :accessor uml-class-operations :initform '())))

(defmethod print-object ((class uml-class) stream)
  (print-unreadable-object (class stream :identity t :type t)
    (format stream "~A ~S ~S"
            (uml-name class)
            (uml-class-attributes class)
            (uml-class-operations class)))
  class)

(defclass uml-note ()
  ((text :initarg :text :accessor uml-note-text)))

(defmethod print-object ((note uml-note) stream)
  (print-unreadable-object (note stream :identity t :type t)
    (format stream "~S" (uml-note-text note)))
  note)

(defclass uml-generalization ()
  ((superclass :initarg :superclass :accessor uml-generalization-superclass)
   (subclass   :initarg :subclass   :accessor uml-generalization-subclass)))

(defmethod print-object ((generalization uml-generalization) stream)
  (print-unreadable-object (generalization stream :identity t :type t)
    (format stream "~A^-~A"
            (uml-generalization-superclass generalization)
            (uml-generalization-subclass generalization)))
  generalization)

(defclass uml-association ()
  ((left-class          :initarg :left-class          :accessor uml-association-left-class)
   (left-multiplicity   :initarg :left-multiplicity   :accessor uml-association-left-multiplicity)
   (left-arrow          :initarg :left-arrow          :accessor uml-association-left-arrow)
   (left-diamond        :initarg :left-diamond        :accessor uml-association-left-diamond)
   (line                :initarg :line                :accessor uml-association-line :initform 'solid)
   (right-class         :initarg :right-class         :accessor uml-association-right-class)
   (right-multiplicity  :initarg :right-multiplicity  :accessor uml-association-right-multiplicity)
   (right-arrow         :initarg :right-arrow         :accessor uml-association-right-arrow)
   (right-diamond       :initarg :right-diamond       :accessor uml-association-right-diamond)))

(defmethod print-object ((association uml-association) stream)
  (let ((*print-circle* nil))
    (print-unreadable-object (association stream :identity t :type t)
      (format stream "[~A]~:[~;<~]~@[~A~]~@[~A~]~A~@[~A~]~@[~A~]~:[~;>~][~A]"
              (uml-association-left-class association)
              (uml-association-left-arrow association)
              (uml-association-left-diamond association)
              (uml-association-left-multiplicity association)
              (case (uml-association-line association)
                ((:solid)  "-")
                ((:dashed) "-.-"))
              (uml-association-right-multiplicity association)
              (uml-association-right-diamond association)
              (uml-association-right-arrow association)
              (uml-association-right-class association))))
  association)


;;; Activity Diagrams

(defclass uml-activity-node ()
  ((transitions-to   :initarg :transitions-to   :accessor uml-transitions-to
                     :initform '())
   (transitions-from :initarg :transitions-from :accessor uml-transitions-from
                     :initform '())))

(defclass uml-activity (uml-named-entity uml-activity-node)
  ())
(defclass uml-decision (uml-named-entity uml-activity-node)
  ())
(defclass uml-parallel (uml-named-entity uml-activity-node)
  ())
(defclass uml-transition ()
  ((from-label :initarg :from-label :accessor uml-transition-from-label)
   (to-label   :initarg :to-label   :accessor uml-transition-to-label)
   (from       :initarg :from       :accessor uml-transition-from
               :type (or uml-activity-node null) :initform nil)
   (to         :initarg :to         :accessor uml-transition-to
               :type (or uml-activity-node null) :initform nil)))

(defmethod add-transition-to ((node uml-activity-node) (transition uml-transition))
  (push transition (uml-transitions-to node)))
(defmethod add-transition-from ((node uml-activity-node) (transition uml-transition))
  (push transition (uml-transitions-from node)))
(defmethod remove-transition-to ((node uml-activity-node) (transition uml-transition))
  (setf (uml-transitions-to node) (delete transition (uml-transitions-to node))))
(defmethod remove-transition-from ((node uml-activity-node) (transition uml-transition))
  (setf (uml-transitions-from node) (delete transition (uml-transitions-from node))))

(defmethod initialize-instance :after ((self uml-transition) &key from to &allow-other-keys)
  (when from
    (add-transition-from from self))
  (when to
    (add-transition-to to self)))

(defmethod (setf uml-transition-from) :after (new-from (self uml-transition))
  (let ((old (uml-transition-from self)))
    (when old
      (remove-transition-from old self)))
  (add-transition-from new-from self))

(defmethod (setf uml-transition-to) :after (new-to (self uml-transition))
  (let ((old (uml-transition-to self)))
    (when old
      (remove-transition-to old self)))
  (add-transition-to new-to self))

(defmethod print-object ((transition uml-transition) stream)
  (let ((*print-circle* nil))
    (print-unreadable-object (transition stream :identity t :type t)
      (format stream "~A ~@[~A~] -> ~@[~A~] ~A"
              (uml-transition-from transition)
              (uml-transition-from-label transition)
              (uml-transition-to-label transition)
              (uml-transition-to transition))))
  transition)

;;; Use Case Diagrams

(defclass uml-actor (uml-named-entity)
  ())

(defmethod print-object ((actor uml-actor) stream)
  (print-unreadable-object (actor stream :identity t :type t)
    (format stream "[~A]" (uml-name actor)))
  actor)

(defclass uml-usecase (uml-named-entity)
  ())

(defmethod print-object ((usecase uml-usecase) stream)
  (print-unreadable-object (usecase stream :identity t :type t)
    (format stream "(~A)" (uml-name usecase)))
  usecase)

(defclass uml-usecase-uses ()
  ((user       :initarg :user       :accessor uml-usecase-user)
   (used       :initarg :used       :accessor uml-usecase-used)))

(defmethod print-object ((asso uml-usecase-uses) stream)
  (print-unreadable-object (asso stream :identity t :type t)
    (format stream "~A \"-\" ~A" (uml-usecase-user asso) (uml-usecase-used asso)))
  asso)

(defclass uml-usecase-extends ()
  ((extending   :initarg :extending    :accessor uml-usecase-extending)
   (extended    :initarg :extended     :accessor uml-usecase-extended)))

(defmethod print-object ((asso uml-usecase-extends) stream)
  (print-unreadable-object (asso stream :identity t :type t)
    (format stream "~A \"<\" ~A" (uml-usecase-extended asso) (uml-usecase-extending asso)))
  asso)

(defclass uml-usecase-includes ()
  ((including    :initarg :including     :accessor uml-usecase-including)
   (included     :initarg :included      :accessor uml-usecase-included)))

(defmethod print-object ((asso uml-usecase-includes) stream)
  (print-unreadable-object (asso stream :identity t :type t)
    (format stream "~A \">\" ~A" (uml-usecase-including asso) (uml-usecase-included asso)))
  asso)

(defclass uml-usecase-generalization ()
  ((subusecase      :initarg :subusecase       :accessor uml-usecase-subusecase)
   (superusecase    :initarg :superusecase     :accessor uml-usecase-superusecase)))

(defmethod print-object ((asso uml-usecase-generalization) stream)
  (print-unreadable-object (asso stream :identity t :type t)
    (format stream "~A \"^\" ~A" (uml-usecase-subusecase asso) (uml-usecase-superusecase asso)))
  asso)

(defclass uml-actor-generalization ()
  ((subactor      :initarg :subactor       :accessor uml-usecase-subactor)
   (superactor    :initarg :superactor     :accessor uml-usecase-superactor)))

(defmethod print-object ((asso uml-actor-generalization) stream)
  (print-unreadable-object (asso stream :identity t :type t)
    (format stream "~A \"^\" ~A" (uml-usecase-subactor asso) (uml-usecase-superactor asso)))
  asso)


;;;; THE END ;;;;
