;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               yuml-parser.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;    
;;;;    Parser for yuml.me files.
;;;;    
;;;;AUTHORS
;;;;    <PJB> Pascal J. Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2016-12-30 <PJB> Created.
;;;;BUGS
;;;;LEGAL
;;;;    AGPL3
;;;;    
;;;;    Copyright Pascal J. Bourguignon 2016 - 2016
;;;;    
;;;;    This program is free software: you can redistribute it and/or modify
;;;;    it under the terms of the GNU Affero General Public License as published by
;;;;    the Free Software Foundation, either version 3 of the License, or
;;;;    (at your option) any later version.
;;;;    
;;;;    This program is distributed in the hope that it will be useful,
;;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;;    GNU Affero General Public License for more details.
;;;;    
;;;;    You should have received a copy of the GNU Affero General Public License
;;;;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;**************************************************************************
(eval-when (:compile-toplevel :load-toplevel :execute)
  (setf *readtable* (copy-readtable nil)))
(defpackage "COM.INFORMATIMAGO.YUML-TO-PLANTUML.YUML-PARSER"
  (:nicknames "YP")
  (:use "COMMON-LISP"
        "COM.INFORMATIMAGO.COMMON-LISP.CESARUM.UTILITY"
        "COM.INFORMATIMAGO.RDP"
        "COM.INFORMATIMAGO.YUML-TO-PLANTUML.UML")
  (:export "PARSE-CLASS-DIAGRAM"
           "PARSE-ACTIVITY-DIAGRAM"
           "PARSE-USECASE-DIAGRAM")
  (:documentation "A parser for yuml.me diagrams."))
(in-package "COM.INFORMATIMAGO.YUML-TO-PLANTUML.YUML-PARSER")


;; (defvar *uml-classes* (make-hash-table :test (function equal)))
;; 
;; (defun reset-uml-classes ()
;;   (setf *uml-classes* (make-hash-table :test (function equal))))
;; 
;; (defun intern-uml-class (name attributes operations)
;;   (let ((old (gethash name *uml-classes*)))
;;     (if old
;;         (cond
;;           (attributes
;;            (error "Ignored attributes ~A in following occurences of class ~A"
;;                   attributes name))
;;           (operations
;;            (error "Ignored operations ~A in following occurences of class ~A"
;;                   operations name))
;;           (t
;;            old))
;;         (setf (gethash name *uml-classes*)
;;               (make-instance 'uml-class
;;                              :name name
;;                              :attributes attributes
;;                              :operations operations)))))

(defgrammar class-diagram
  :scanner t
  :skip-spaces nil
  :trace nil
  :start diagram
  :terminals (
              (note-text           "note:[^\\]]*")

              (identifier          "[A-Za-z_][-A-Za-z_0-9 ]*")
              (integer             "[0-9]+")
              
              (left-arrow          "<" / ">")
              (right-arrow         ">")
              (odiamond            "<>")
              (diamond             "\\+\\+")
              (solid               "-+" / "\\.")
              (inherit             "\\^")
              (dashed              "-+\\(\\.-+\\)+")
              (line-label          "\\(\\[[^\\]]+\\]\\|[][a-zA-Z_0-9 ]+\\)")
              (star                "\\*")

              (return              #.(string (code-char 13)))
              (linefeed            #.(string (code-char 10)))
              (white-space         #.(format nil " ~C" #\tab))
              
              )
  
  :rules ((--> diagram             (seq line
                                        ;; (rep  eol  (opt line) :action line)
                                        ;; eol is not read by the current
                                        ;; buffered-scanner or rdp-scanner classes.
                                        (rep line :action line)
                                        :action (cons line $2)))

          (--> line                (seq spaces phrase (rep "," spaces phrase :action phrase) spaces 
                                        :action (cons phrase $3)))

          (--> phrase              (seq class-desc (rep spaces association-desc spaces class-desc
                                                        :action (list association-desc class-desc))
                                        :action (if $2
                                                    (loop :for (left asso right)
                                                            :on (cons class-desc (apply (function append) $2))
                                                          :by (function cddr)
                                                          :while asso
                                                          :collect (cond
                                                                     ((eq asso :inherit)
                                                                      (make-instance 'uml-generalization
                                                                                     :superclass right
                                                                                     :subclass left))
                                                                     ((eq asso :generalize)
                                                                      (make-instance 'uml-generalization
                                                                                     :superclass left
                                                                                     :subclass right))
                                                                     (t (setf (uml-association-left-class asso) left
                                                                              (uml-association-right-class asso) right)
                                                                        asso)))
                                                    class-desc)))

          (--> class-desc          (seq "["
                                        (alt (seq note-text
                                                  :action (make-instance 'uml-note :text (subseq (second note-text) 5)))
                                             (seq identifier
                                                  (opt "|"
                                                       (opt attribute-list :action attribute-list)
                                                       (opt "|" operation-list :action operation-list)
                                                       :action (cons $2 $3))
                                                  :action (intern-object 'uml-class
                                                                         :name       (second identifier)
                                                                         :attributes (first  $2)
                                                                         :operations (second $2))))
                                        "]"
                                        :action $2))
          
          (--> attribute-list      (seq attribute (rep ";" attribute :action attribute)
                                        :action (cons attribute $2)))
          (--> attribute           (seq identifier (opt ":" type :action type)
                                        :action (list (second identifier) :type (or (first $2) 't))))

          (--> operation-list      (seq operation (rep ";" operation :action operation)
                                        :action (cons operation $2)))
          (--> operation           (seq identifier (opt "(" (opt parameter-list) ")" :action $2)
                                        :action (cons (second identifier) $2)))
          (--> parameter-list      (seq parameter  (rep "," parameter :action parameter)
                                        :action (cons parameter $2)))
          (--> parameter           (seq identifier (opt ":" type :action type)
                                        :action (list (second identifier) :type (or (first $2) 't))))
          (--> type                (seq identifier :action (second identifier)))

          
          (--> association-desc
               ;; -------    solid
               ;; <>-----  + odiamond
               ;; -.-.-.-  = dashed
               ;; ++-----  & diamond
               (seq 
                (opt left-arrow)
                (opt multiplicity)
                (alt
                 (seq diamond  (alt (seq solid  :action :solid)
                                    (seq dashed :action :dashed))
                      :action (list $2 :left :diamond))
                 (seq odiamond (alt (seq solid  :action :solid)
                                    (seq dashed :action :dashed))
                      :action (list $2 :left :odiamond))
                 (seq inherit  (opt solid)
                      :action :generalize)
                 (seq solid    (opt (alt (seq odiamond :action :odiamond)
                                         (seq diamond  :action :diamond)
                                         (seq inherit  :action :inherit)))
                      :action (if (eq :inherit (first $2))
                                  :inherit
                                  (cons :solid (when $2 (list :right (first $2))))))
                 (seq dashed
                      :action (list :dashed)))
                (opt multiplicity)
                (opt right-arrow)
                :action (if (member $3 '(:inherit :generalize))
                            $3
                            (let ((left-diamond   (getf (rest $3) :left))
                                  (line           (first $3))
                                  (right-diamond  (getf (rest $3) :right)))
                              (make-instance 'uml-association
                                             :left-class nil
                                             :left-arrow (first $1)
                                             :left-multiplicity (first $2)
                                             :left-diamond left-diamond
                                             :line line
                                             :right-diamond right-diamond
                                             :right-multiplicity (first $4)
                                             :right-arrow (first $5)
                                             :right-class nil)))))
          
          (--> multiplicity        (alt (seq value (opt  ".." (alt value
                                                                   (seq star :action "*"))
                                                         :action $2)
                                             :action (if $2
                                                         (cons value $2)
                                                         value)) 
                                        (seq star :action  "*")))
          (--> value               (seq integer :action (second integer)))

          ;; --
          (--> spaces              (rep (alt white-space continuation)))
          (--> continuation        "\\" eol)
          (--> eol                 (alt (seq return (opt linefeed))
                                        linefeed))))

(defgrammar activity-diagram
  :scanner t
  :skip-spaces nil
  :trace nil
  :start diagram
  :terminals (

              (activity            "\\([^\\)]+\\)")
              (parallel            "\\|[^\\|]+\\|")
              ;; (object              "\\[[^\\]]+\\]")
              (decision            "<[^<>]+>")

              (left-arrow          "<")
              (right-arrow         ">")
              (solid               "-+" / "\\.")
              (bracketed-line-label   "\\[[^\\]]+\\]")
              (unbracketed-line-label "[a-zA-Z_0-9 ]+")

              
              (return              #.(string (code-char 13)))
              (linefeed            #.(string (code-char 10)))
              (white-space         #.(format nil " ~C" #\tab))

              )
  
  :rules ((--> diagram             (seq line
                                        ;; (rep  eol  (opt line) :action line)
                                        ;; eol is not read by the current
                                        ;; buffered-scanner or rdp-scanner classes.
                                        (rep line :action line)
                                        :action (cons line $2)))

          (--> line                (seq phrase spaces (rep "," phrase spaces :action phrase)
                                        :action (cons phrase $3)))
          
          (--> phrase              (seq node (rep (opt connector) node :action (list (first $1) $2))
                                        :action (if $2
                                                    (loop :with left     := node
                                                          :for item :in $2
                                                          :collect (if (atom item)
                                                                       (prog1 left
                                                                         (setf left item))
                                                                       (destructuring-bind ((connector to-label from-label) right) item
                                                                         (prog1 (ecase connector
                                                                                  (:left  (make-instance 'uml-transition
                                                                                                         :to-label to-label
                                                                                                         :from-label from-label
                                                                                                         :to left
                                                                                                         :from right))
                                                                                  (:right (make-instance 'uml-transition
                                                                                                         :to-label to-label
                                                                                                         :from-label from-label
                                                                                                         :to right
                                                                                                         :from left)))
                                                                           (setf left right))))) 
                                                    node)))

          (--> node                (alt (seq parallel :action (intern-object 'uml-parallel :name (string-trim "|"  (second parallel))))
                                        (seq decision :action (intern-object 'uml-decision :name (string-trim "<>" (second decision))))
                                        (seq activity :action (intern-object 'uml-activity :name (string-trim "()" (second activity))))
                                        #|object|#)) 

          (--> connector           (alt (seq left-arrow (opt line-label) solid (opt line-label)
                                             :action (list :left (first $2) (first $4)))
                                        (seq (opt line-label) solid (opt line-label) right-arrow
                                             :action (list :right (first $3) (first $1)))))

          (--> line-label          (seq (alt (seq bracketed-line-label   :action (make-instance 'uml-bracketed-label :label (second bracketed-line-label)))
                                             (seq unbracketed-line-label :action (make-instance 'uml-label           :label (second unbracketed-line-label))))
                                        :action $1))
          
          ;; -
          (--> spaces              (rep (alt white-space continuation)))
          (--> continuation        "\\" eol)
          (--> eol                 (alt (seq return (opt linefeed))
                                        linefeed))))

(defgrammar usecase-diagram
  :scanner t
  :skip-spaces nil
  :trace nil
  :start diagram
  :terminals (

              (note                "\\(note:[^\\)]*\\)")
              (usecase             "\\([^\\)]+\\)")
              (actor               "\\[[^\\]]+\\]")

              (extends             "<")
              (includes            ">")
              (inherits            "\\^")
              (uses                "-")
              
              (white-space         #.(format nil " ~C" #\tab))
              )
  
  :rules ((--> diagram             (seq line
                                        ;; (rep  eol  (opt line) :action line)
                                        ;; eol is not read by the current
                                        ;; buffered-scanner or rdp-scanner classes.
                                        (rep line :action line)
                                        :action (cons line $2)))

          (--> line                (seq phrase spaces
                                        (rep "," phrase spaces :action phrase)
                                        :action (apply (function append) (cons phrase $3))))
          
          (--> phrase              (seq node
                                        (rep connector node :action (list connector node))
                                        :action (if $2
                                                    (loop :for (left asso right)
                                                            :on (cons node (apply (function append) $2))
                                                          :by (function cddr)
                                                          :while asso
                                                          :collect (ecase asso
                                                                     ((:inherits)
                                                                      (cond ((and (typep left  'uml-usecase)
                                                                                  (typep right 'uml-usecase))
                                                                             (make-instance 'uml-usecase-generalization
                                                                                            :superusecase right
                                                                                            :subusecase left))
                                                                            ((and (typep left  'uml-actor)
                                                                                  (typep right 'uml-actor))
                                                                             (make-instance 'uml-actor-generalization
                                                                                            :superactor right
                                                                                            :subactor left))
                                                                            (t
                                                                             (error "Incompatible entities for inherits association: ~A vs. ~A~%~S~%~S"
                                                                                    (class-name (class-of left))
                                                                                    (class-name (class-of right))
                                                                                    left right))))
                                                                     ((:extends)
                                                                      (make-instance 'uml-usecase-extends
                                                                                     :extending right
                                                                                     :extended left))
                                                                     ((:includes)
                                                                      (make-instance 'uml-usecase-includes
                                                                                     :including left
                                                                                     :included right))
                                                                     ((:uses)
                                                                      (make-instance 'uml-usecase-uses
                                                                                     :user left
                                                                                     :used right))))
                                                    (list node))))

          (--> node                (seq spaces (alt (seq note    :action (make-instance 'uml-note    :text (string-trim "()" (second note))))
                                                    (seq usecase :action (intern-object 'uml-usecase :name (string-trim "()" (second usecase))))
                                                    (seq actor   :action (intern-object 'uml-actor   :name (string-trim "[]" (second actor)))))
                                        :action $2)) 

          (--> connector           (seq spaces (alt (seq extends  :action :extends)
                                                    (seq includes :action :includes)
                                                    (seq inherits :action :inherits)
                                                    (seq uses     :action :uses))
                                        :action $2))
          
          ;; -
          (--> spaces              (rep white-space))))


;;;; THE END ;;;;
